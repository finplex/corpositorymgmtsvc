package com.finplex.corpositorymgmtsvc;

import io.swagger.v3.core.util.Json;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/corposmgmtsvc")
public class Controller {
    @PostMapping(value = "/login")
    public ResponseEntity<String> CorpositoryLogin(@RequestHeader("x-api-key") String apiKey) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("request","login");
        JSONObject parajson = new JSONObject();
        parajson.put("user-id","ashwin@finplex.in");
        parajson.put("password","Finplex@100");
        reqbody.put("para",parajson);

        String url = "https://test-client-api.corpository.com/clientapi/api/demo/";
        HttpEntity<String> entity = new HttpEntity<String>(reqbody.toString(),headers);

        String response = restTemplate.postForObject(url,entity, String.class);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping(value = "/searchcompanies")
    public ResponseEntity<String> searchCompanies (@RequestHeader("x-api-key") String apiKey, @RequestParam List<String> companiesname) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("request","searchCompanies");
        JSONObject parajson = new JSONObject();
        parajson.put("api_auth_token","ISUAYrtWXFxMEGY9CKHCsM9d82OK75Pp");
        parajson.put("user_id","1542");
        parajson.put("source_system","clientapi");
        JSONArray empty = new JSONArray();
        JSONObject search_criteria = new JSONObject();
        search_criteria.put("company-ids",empty);
        search_criteria.put("company-names",empty);
        JSONArray compNames =  new JSONArray(companiesname);
        search_criteria.put("company-name-partials", compNames);
        search_criteria.put("cins",empty);
        search_criteria.put("cin-partials",empty);
        search_criteria.put("partials-search-type","");
        search_criteria.put("city",empty);
        search_criteria.put("state",empty);
        search_criteria.put("status",empty);
        search_criteria.put("type",empty);
        search_criteria.put("liability",empty);
        search_criteria.put("offset-start",0);
        search_criteria.put("offset-end",10);

        parajson.put("search-criteria",search_criteria);

        reqbody.put("para",parajson);

        String url = "https://test-client-api.corpository.com/clientapi/api/demo/";
        HttpEntity<String> entity = new HttpEntity<String>(reqbody.toString(),headers);

        String response = restTemplate.postForObject(url,entity, String.class);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/placeorder")
    public ResponseEntity<String>  placeOrder (@RequestHeader("x-api-key") String apiKey, @RequestParam List<JSONObject> companydata) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("request","creditOrder");
        JSONObject parajson = new JSONObject();
        parajson.put("api_auth_token","ISUAYrtWXFxMEGY9CKHCsM9d82OK75Pp");
        parajson.put("user_id","1542");

        JSONObject webhookjson = new JSONObject();
        webhookjson.put("web-hook-url","https://domain.com/1.0/webhook_report");
        webhookjson.put("key-name","webhook_token");
        webhookjson.put("key-value","webhook_value");

        JSONArray webhookarray = new JSONArray();
        webhookarray.put(webhookjson);
        parajson.put("webhook_urls",webhookarray);

        JSONArray comparray = new JSONArray(companydata);
        parajson.put("company_data",comparray);
        parajson.put("source_system","clientapi");

        reqbody.put("para",parajson);

        String url = "https://test-client-api.corpository.com/clientapi/api/demo/";

        HttpEntity<String> entity = new HttpEntity<String>(reqbody.toString(),headers);

        String response = restTemplate.postForObject(url,entity, String.class);
//        String response = "ok";
        return ResponseEntity.ok().body(response);
//        {"work-order-details":[{"company-id":455,"reference-id":44341},{"company-id":4060,"reference-id":44342}],"message":"success","in_process_companies":{"companies":null,"status":null,"message":null},"status":1}
    }

    @PostMapping("/companyprofile")
    public ResponseEntity<String>  companyProfile (@RequestHeader("x-api-key") String apiKey, @RequestParam List<Integer> companyid,@RequestParam Integer referenceid) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("api_auth_token","ISUAYrtWXFxMEGY9CKHCsM9d82OK75Pp");
        reqbody.put("user_id","1542");
        JSONArray compid = new JSONArray(companyid);
        reqbody.put("company_ids",compid);
        reqbody.put("reference_id",referenceid);

        HttpEntity<String> entity = new HttpEntity<String>(reqbody.toString(),headers);
        String url = "https://test-client-api.corpository.com/clientapi/api/demo/companyProfile";
//        String response = restTemplate.postForObject(url,entity, String.class);
        String response = "ok";
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/directorlist")
    public ResponseEntity<String>  directorList (@RequestHeader("x-api-key") String apiKey, @RequestParam List<Integer> companyid,@RequestParam Integer referenceid) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("api_auth_token","ISUAYrtWXFxMEGY9CKHCsM9d82OK75Pp");
        reqbody.put("user_id","1542");
        JSONArray compid = new JSONArray(companyid);
        reqbody.put("company_ids",compid);
        reqbody.put("reference_id",referenceid);
        HttpEntity<String> entity = new HttpEntity<String>(reqbody.toString(),headers);
        String url = "https://test-client-api.corpository.com/clientapi/api/demo/directorList";
//        String response = restTemplate.postForObject(url,entity, String.class);
        String response = "ok";
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/directordetails")
    public ResponseEntity<String>  directorDetails (@RequestHeader("x-api-key") String apiKey, @RequestParam List<Integer> companyid,@RequestParam Integer referenceid,@RequestParam List<String> directorid) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("api_auth_token","ISUAYrtWXFxMEGY9CKHCsM9d82OK75Pp");
        reqbody.put("user_id","1542");
        JSONArray compid = new JSONArray(companyid);
        reqbody.put("company_ids",compid);
        reqbody.put("reference_id",referenceid);
        JSONArray dinids = new JSONArray(directorid);
        reqbody.put("din",dinids);
        HttpEntity<String> entity = new HttpEntity<String>(reqbody.toString(),headers);
        String url = "https://test-client-api.corpository.com/clientapi/api/demo/directorList";
//        String response = restTemplate.postForObject(url,entity, String.class);
        String response = "ok";
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/chargedetails")
    public ResponseEntity<String>  chargeDetails (@RequestHeader("x-api-key") String apiKey, @RequestParam List<Integer> companyid,@RequestParam Integer referenceid) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        JSONObject reqbody = new JSONObject();
        reqbody.put("api_auth_token","ISUAYrtWXFxMEGY9CKHCsM9d82OK75Pp");
        reqbody.put("user_id","1542");

        JSONArray compid = new JSONArray(companyid);
        JSONObject charge_criteria = new JSONObject();
        charge_criteria.put("company_ids",compid);
        charge_criteria.put("offset-start",0);
        charge_criteria.put("offset-end",2000);

        reqbody.put("charge_criteria",charge_criteria);
        reqbody.put("reference_id",referenceid);

        String url = "https://test-client-api.corpository.com/clientapi/api/demo/chargeDetails";
//        String response = restTemplate.postForObject(url,entity, String.class);
        String response = "ok";
        return ResponseEntity.ok().body(response);
    }
}
